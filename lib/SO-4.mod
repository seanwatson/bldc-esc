PCBNEW-LibModule-V1  
# encoding utf-8
Units mm
$INDEX
SO-4
$EndINDEX
$MODULE SO-4
Po 0 0 0 15 00000000 00000000 ~~
Li SO-4
Cd 
Sc 00000000
At SMD
Op 0 0 0
.SolderMask 0
.SolderPaste 0
T0 0.635 -2.54 1 1 0 0.05 N V 21 "SO-4"
T1 1.27 3.81 1 1 0 0.05 N V 21 "VAL**"
DS -2.0849 2.4384 2.0849 2.4384 0.2032 21
DS 2.0849 2.4384 2.0849 -1.1684 0.2032 21
DS 2.0849 -1.1684 -2.0849 -1.1684 0.2032 21
DS -2.0849 -1.1684 -2.0849 2.4384 0.2032 21
DC -1.285 -0.354 -0.9501 -0.354 0.1016 21
DP 0 0 0 0 4 0.381 24
Dl -3.471 -0.8255
Dl -2.15 -0.8255
Dl -2.15 -0.4445
Dl -3.471 -0.4445
DP 0 0 0 0 4 0.381 24
Dl -3.471 1.7145
Dl -2.15 1.7145
Dl -2.15 2.0955
Dl -3.471 2.0955
DP 0 0 0 0 4 0.381 24
Dl 3.471 2.0955
Dl 2.15 2.0955
Dl 2.15 1.7145
Dl 3.471 1.7145
DP 0 0 0 0 4 0.381 24
Dl 3.471 -0.4445
Dl 2.15 -0.4445
Dl 2.15 -0.8255
Dl 3.471 -0.8255
$PAD
Sh "1" R 1 0.6 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -3.24 -0.635
$EndPAD
$PAD
Sh "2" R 1 0.6 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -3.24 1.905
$EndPAD
$PAD
Sh "3" R 1 0.6 0 0 1800
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 3.24 1.905
$EndPAD
$PAD
Sh "4" R 1 0.6 0 0 1800
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 3.24 -0.635
$EndPAD
$EndMODULE SO-4
