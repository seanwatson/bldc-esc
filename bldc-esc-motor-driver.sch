EESchema Schematic File Version 4
LIBS:bldc-esc-cache
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 5 9
Title "BLDC ESC"
Date "2017-06-18"
Rev "A0"
Comp "Sean Watson"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L drv8323r:drv8323rs U501
U 1 1 593C9612
P 4800 2850
F 0 "U501" H 4150 4300 50  0000 C CNN
F 1 "drv8323rs" H 5200 1550 50  0000 C CNN
F 2 "Package_DFN_QFN:QFN-48-1EP_7x7mm_P0.5mm_EP5.3x5.3mm_ThermalVias" H 5150 1750 29  0001 C CNN
F 3 "" H 5150 1750 29  0000 C CNN
	1    4800 2850
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C508
U 1 1 593C9776
P 4350 4550
F 0 "C508" H 4360 4620 50  0000 L CNN
F 1 "1u" H 4360 4470 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 4350 4550 50  0001 C CNN
F 3 "" H 4350 4550 50  0000 C CNN
	1    4350 4550
	-1   0    0    -1  
$EndComp
$Comp
L power:VCC #PWR0503
U 1 1 593C99A3
P 3600 750
F 0 "#PWR0503" H 3600 600 50  0001 C CNN
F 1 "VCC" H 3600 900 50  0000 C CNN
F 2 "" H 3600 750 50  0000 C CNN
F 3 "" H 3600 750 50  0000 C CNN
	1    3600 750 
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C506
U 1 1 593C9A13
P 3800 1100
F 0 "C506" H 3810 1170 50  0000 L CNN
F 1 "100n" H 3810 1020 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 3800 1100 50  0001 C CNN
F 3 "" H 3800 1100 50  0000 C CNN
	1    3800 1100
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR0507
U 1 1 593C9FB8
P 4400 800
F 0 "#PWR0507" H 4400 650 50  0001 C CNN
F 1 "+3.3V" H 4400 940 50  0000 C CNN
F 2 "" H 4400 800 50  0000 C CNN
F 3 "" H 4400 800 50  0000 C CNN
	1    4400 800 
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C509
U 1 1 593CA00C
P 4600 1100
F 0 "C509" H 4610 1170 50  0000 L CNN
F 1 "1u" H 4610 1020 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric" H 4600 1100 50  0001 C CNN
F 3 "" H 4600 1100 50  0000 C CNN
	1    4600 1100
	1    0    0    -1  
$EndComp
$Comp
L Device:CP1_Small C505
U 1 1 593CA158
P 3600 1100
F 0 "C505" H 3610 1170 50  0000 L CNN
F 1 "100u" H 3610 1020 50  0000 L CNN
F 2 "Capacitor_SMD:CP_Elec_10x10.5" H 3600 1100 50  0001 C CNN
F 3 "" H 3600 1100 50  0000 C CNN
	1    3600 1100
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C510
U 1 1 593CA236
P 4800 1100
F 0 "C510" H 4810 1170 50  0000 L CNN
F 1 "47n" H 4810 1020 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 4800 1100 50  0001 C CNN
F 3 "" H 4800 1100 50  0000 C CNN
	1    4800 1100
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C511
U 1 1 593CA33D
P 5050 1100
F 0 "C511" H 5060 1170 50  0000 L CNN
F 1 "100n" H 5060 1020 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 5050 1100 50  0001 C CNN
F 3 "" H 5050 1100 50  0000 C CNN
	1    5050 1100
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C513
U 1 1 593CA5D8
P 7350 1250
F 0 "C513" H 7360 1320 50  0000 L CNN
F 1 "100n" H 7360 1170 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 7350 1250 50  0001 C CNN
F 3 "" H 7350 1250 50  0000 C CNN
	1    7350 1250
	1    0    0    -1  
$EndComp
$Comp
L Device:CP1_Small C512
U 1 1 593CA5DE
P 7000 1250
F 0 "C512" H 7010 1320 50  0000 L CNN
F 1 "100u" H 7010 1170 50  0000 L CNN
F 2 "Capacitor_SMD:CP_Elec_6.3x7.7" H 7000 1250 50  0001 C CNN
F 3 "" H 7000 1250 50  0000 C CNN
	1    7000 1250
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R508
U 1 1 593CA73A
P 6550 1100
F 0 "R508" H 6580 1120 50  0000 L CNN
F 1 "33k" H 6580 1060 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 6550 1100 50  0001 C CNN
F 3 "" H 6550 1100 50  0000 C CNN
	1    6550 1100
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R509
U 1 1 593CA7E9
P 6550 1450
F 0 "R509" H 6580 1470 50  0000 L CNN
F 1 "10k" H 6580 1410 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 6550 1450 50  0001 C CNN
F 3 "" H 6550 1450 50  0000 C CNN
	1    6550 1450
	1    0    0    -1  
$EndComp
$Comp
L power:GNDD #PWR0509
U 1 1 593CAC53
P 6550 1650
F 0 "#PWR0509" H 6550 1400 50  0001 C CNN
F 1 "GNDD" H 6550 1500 50  0000 C CNN
F 2 "" H 6550 1650 50  0000 C CNN
F 3 "" H 6550 1650 50  0000 C CNN
	1    6550 1650
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR0510
U 1 1 593CB075
P 8200 850
F 0 "#PWR0510" H 8200 700 50  0001 C CNN
F 1 "+3.3V" H 8200 990 50  0000 C CNN
F 2 "" H 8200 850 50  0000 C CNN
F 3 "" H 8200 850 50  0000 C CNN
	1    8200 850 
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C507
U 1 1 593CB115
P 4100 1100
F 0 "C507" H 4110 1170 50  0000 L CNN
F 1 "4.7u" H 4110 1020 50  0000 L CNN
F 2 "Capacitor_SMD:C_1210_3225Metric" H 4100 1100 50  0001 C CNN
F 3 "" H 4100 1100 50  0000 C CNN
	1    4100 1100
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R513
U 1 1 593CD60D
P 9150 5150
F 0 "R513" H 9180 5170 50  0000 L CNN
F 1 "1m" H 9180 5110 50  0000 L CNN
F 2 "lib:R_3921" H 9150 5150 50  0001 C CNN
F 3 "" H 9150 5150 50  0000 C CNN
	1    9150 5150
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R512
U 1 1 593CD6E4
P 8150 4850
F 0 "R512" H 8180 4870 50  0000 L CNN
F 1 "1m" H 8180 4810 50  0000 L CNN
F 2 "lib:R_3921" H 8150 4850 50  0001 C CNN
F 3 "" H 8150 4850 50  0000 C CNN
	1    8150 4850
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R510
U 1 1 593CD78A
P 7200 4550
F 0 "R510" H 7230 4570 50  0000 L CNN
F 1 "1m" H 7230 4510 50  0000 L CNN
F 2 "lib:R_3921" H 7200 4550 50  0001 C CNN
F 3 "" H 7200 4550 50  0000 C CNN
	1    7200 4550
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR0511
U 1 1 593D0FD2
P 9150 1400
F 0 "#PWR0511" H 9150 1250 50  0001 C CNN
F 1 "VCC" H 9150 1550 50  0000 C CNN
F 2 "" H 9150 1400 50  0000 C CNN
F 3 "" H 9150 1400 50  0000 C CNN
	1    9150 1400
	1    0    0    -1  
$EndComp
Text HLabel 2650 1850 0    28   Input ~ 0
ENABLE
Text HLabel 2650 1950 0    28   Input ~ 0
NSHDN
Text HLabel 2650 2150 0    28   Output ~ 0
NFAULT
Text HLabel 2650 2350 0    28   Output ~ 0
SDO
Text HLabel 2650 2450 0    28   Input ~ 0
SDI
Text HLabel 2650 2550 0    28   Input ~ 0
SCLK
Text HLabel 2650 2650 0    28   Input ~ 0
NSCS
Text HLabel 2650 2850 0    28   Input ~ 0
INHA
Text HLabel 2650 2950 0    28   Input ~ 0
INLA
Text HLabel 2650 3050 0    28   Input ~ 0
INHB
Text HLabel 2650 3150 0    28   Input ~ 0
INLB
Text HLabel 2650 3250 0    28   Input ~ 0
INHC
Text HLabel 2650 3350 0    28   Input ~ 0
INLC
Text HLabel 2650 3550 0    28   Output ~ 0
SOA
Text HLabel 2650 3650 0    28   Output ~ 0
SOB
Text HLabel 2650 3750 0    28   Output ~ 0
SOC
$Comp
L Device:C_Small C514
U 1 1 593D6425
P 10100 4650
F 0 "C514" H 10110 4720 50  0000 L CNN
F 1 "100n" H 10110 4570 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 10100 4650 50  0001 C CNN
F 3 "" H 10100 4650 50  0000 C CNN
	1    10100 4650
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C515
U 1 1 593D6AF9
P 10400 4650
F 0 "C515" H 10410 4720 50  0000 L CNN
F 1 "100n" H 10410 4570 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 10400 4650 50  0001 C CNN
F 3 "" H 10400 4650 50  0000 C CNN
	1    10400 4650
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C516
U 1 1 593D6B7A
P 10700 4650
F 0 "C516" H 10710 4720 50  0000 L CNN
F 1 "100n" H 10710 4570 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 10700 4650 50  0001 C CNN
F 3 "" H 10700 4650 50  0000 C CNN
	1    10700 4650
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR0513
U 1 1 593D6C0A
P 10100 4350
F 0 "#PWR0513" H 10100 4200 50  0001 C CNN
F 1 "VCC" H 10100 4500 50  0000 C CNN
F 2 "" H 10100 4350 50  0000 C CNN
F 3 "" H 10100 4350 50  0000 C CNN
	1    10100 4350
	1    0    0    -1  
$EndComp
Text Notes 10250 4400 0    28   ~ 0
Place near MOSFET drains\n
$Comp
L Connector_Generic:Conn_01x03 P502
U 1 1 593D78E3
P 10850 3000
F 0 "P502" H 10850 3200 50  0000 C CNN
F 1 "CONN_01X03" V 10950 3000 50  0000 C CNN
F 2 "Connector_PinHeader_1.00mm:PinHeader_1x03_P1.00mm_Vertical" H 10850 3000 50  0001 C CNN
F 3 "" H 10850 3000 50  0000 C CNN
	1    10850 3000
	1    0    0    -1  
$EndComp
$Comp
L power:GNDPWR #PWR0512
U 1 1 593F3989
P 9600 5500
F 0 "#PWR0512" H 9600 5300 50  0001 C CNN
F 1 "GNDPWR" H 9600 5370 50  0000 C CNN
F 2 "" H 9600 5450 50  0000 C CNN
F 3 "" H 9600 5450 50  0000 C CNN
	1    9600 5500
	1    0    0    -1  
$EndComp
$Comp
L power:GNDPWR #PWR0517
U 1 1 593F3DAE
P 10700 4950
F 0 "#PWR0517" H 10700 4750 50  0001 C CNN
F 1 "GNDPWR" H 10700 4820 50  0000 C CNN
F 2 "" H 10700 4900 50  0000 C CNN
F 3 "" H 10700 4900 50  0000 C CNN
	1    10700 4950
	1    0    0    -1  
$EndComp
$Comp
L power:GNDPWR #PWR0506
U 1 1 593F450F
P 4350 4950
F 0 "#PWR0506" H 4350 4750 50  0001 C CNN
F 1 "GNDPWR" H 4350 4820 50  0000 C CNN
F 2 "" H 4350 4900 50  0000 C CNN
F 3 "" H 4350 4900 50  0000 C CNN
	1    4350 4950
	1    0    0    -1  
$EndComp
$Comp
L power:GNDPWR #PWR0504
U 1 1 593F4E0C
P 3800 1400
F 0 "#PWR0504" H 3800 1200 50  0001 C CNN
F 1 "GNDPWR" H 3800 1270 50  0000 C CNN
F 2 "" H 3800 1350 50  0000 C CNN
F 3 "" H 3800 1350 50  0000 C CNN
	1    3800 1400
	1    0    0    -1  
$EndComp
$Comp
L power:GNDPWR #PWR0516
U 1 1 593F5A18
P 10550 6050
F 0 "#PWR0516" H 10550 5850 50  0001 C CNN
F 1 "GNDPWR" H 10550 5920 50  0000 C CNN
F 2 "" H 10550 6000 50  0000 C CNN
F 3 "" H 10550 6000 50  0000 C CNN
	1    10550 6050
	1    0    0    -1  
$EndComp
$Comp
L power:GNDD #PWR0514
U 1 1 593F5A7A
P 10200 6050
F 0 "#PWR0514" H 10200 5800 50  0001 C CNN
F 1 "GNDD" H 10200 5900 50  0000 C CNN
F 2 "" H 10200 6050 50  0000 C CNN
F 3 "" H 10200 6050 50  0000 C CNN
	1    10200 6050
	1    0    0    -1  
$EndComp
Text Notes 10150 5800 0    29   ~ 0
Connect power and digital\ngrounds in one spot only\n
$Comp
L power:GNDPWR #PWR0508
U 1 1 593F63AE
P 5700 1500
F 0 "#PWR0508" H 5700 1300 50  0001 C CNN
F 1 "GNDPWR" H 5700 1370 50  0000 C CNN
F 2 "" H 5700 1450 50  0000 C CNN
F 3 "" H 5700 1450 50  0000 C CNN
	1    5700 1500
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R505
U 1 1 5944C731
P 3400 6100
F 0 "R505" H 3430 6120 50  0000 L CNN
F 1 "2.2k" H 3430 6060 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 3400 6100 50  0001 C CNN
F 3 "" H 3400 6100 50  0000 C CNN
	1    3400 6100
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R503
U 1 1 5944D071
P 3150 6100
F 0 "R503" H 3180 6120 50  0000 L CNN
F 1 "2.2k" H 3180 6060 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 3150 6100 50  0001 C CNN
F 3 "" H 3150 6100 50  0000 C CNN
	1    3150 6100
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R506
U 1 1 5944D106
P 3650 6100
F 0 "R506" H 3680 6120 50  0000 L CNN
F 1 "2.2k" H 3680 6060 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 3650 6100 50  0001 C CNN
F 3 "" H 3650 6100 50  0000 C CNN
	1    3650 6100
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R507
U 1 1 5944E21D
P 3900 6100
F 0 "R507" H 3930 6120 50  0000 L CNN
F 1 "10k" H 3930 6060 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 3900 6100 50  0001 C CNN
F 3 "" H 3900 6100 50  0000 C CNN
	1    3900 6100
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR0505
U 1 1 5944E666
P 3900 5850
F 0 "#PWR0505" H 3900 5700 50  0001 C CNN
F 1 "+3.3V" H 3900 5990 50  0000 C CNN
F 2 "" H 3900 5850 50  0000 C CNN
F 3 "" H 3900 5850 50  0000 C CNN
	1    3900 5850
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C502
U 1 1 5944F79B
P 1900 6900
F 0 "C502" H 1910 6970 50  0000 L CNN
F 1 "4.7n" H 1910 6820 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 1900 6900 50  0001 C CNN
F 3 "" H 1900 6900 50  0000 C CNN
	1    1900 6900
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C501
U 1 1 5944F8A4
P 1700 6900
F 0 "C501" H 1710 6970 50  0000 L CNN
F 1 "4.7n" H 1710 6820 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 1700 6900 50  0001 C CNN
F 3 "" H 1700 6900 50  0000 C CNN
	1    1700 6900
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C503
U 1 1 5944F95A
P 2100 6900
F 0 "C503" H 2110 6970 50  0000 L CNN
F 1 "4.7n" H 2110 6820 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 2100 6900 50  0001 C CNN
F 3 "" H 2100 6900 50  0000 C CNN
	1    2100 6900
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C504
U 1 1 5944FA3B
P 2300 6900
F 0 "C504" H 2310 6970 50  0000 L CNN
F 1 "4.7n" H 2310 6820 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 2300 6900 50  0001 C CNN
F 3 "" H 2300 6900 50  0000 C CNN
	1    2300 6900
	1    0    0    -1  
$EndComp
$Comp
L power:GNDD #PWR0501
U 1 1 5945182F
P 2300 7200
F 0 "#PWR0501" H 2300 6950 50  0001 C CNN
F 1 "GNDD" H 2300 7050 50  0000 C CNN
F 2 "" H 2300 7200 50  0000 C CNN
F 3 "" H 2300 7200 50  0000 C CNN
	1    2300 7200
	1    0    0    -1  
$EndComp
Text HLabel 1400 6400 0    28   Output ~ 0
HALLA
Text HLabel 1400 6500 0    28   Output ~ 0
HALLB
Text HLabel 1400 6600 0    28   Output ~ 0
HALLC
Text HLabel 1400 6100 0    28   Output ~ 0
MTEMP
$Comp
L Connector_Generic:Conn_01x06 P501
U 1 1 5945349E
P 4250 6650
F 0 "P501" H 4250 7000 50  0000 C CNN
F 1 "CONN_01X06" V 4350 6650 50  0000 C CNN
F 2 "Connector_PinHeader_1.00mm:PinHeader_1x06_P1.00mm_Vertical" H 4250 6650 50  0001 C CNN
F 3 "" H 4250 6650 50  0000 C CNN
	1    4250 6650
	1    0    0    1   
$EndComp
NoConn ~ 3900 2050
$Comp
L Device:R_Small R511
U 1 1 594C9CDC
P 7850 1100
F 0 "R511" H 7880 1120 50  0000 L CNN
F 1 "75" H 7880 1060 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 7850 1100 50  0001 C CNN
F 3 "" H 7850 1100 50  0000 C CNN
	1    7850 1100
	1    0    0    -1  
$EndComp
$Comp
L Device:LED_Small D502
U 1 1 594C9FA9
P 7850 1400
F 0 "D502" H 7800 1525 50  0000 L CNN
F 1 "3v3" H 7675 1300 50  0000 L CNN
F 2 "LED_SMD:LED_0603_1608Metric" V 7850 1400 50  0001 C CNN
F 3 "" V 7850 1400 50  0000 C CNN
	1    7850 1400
	0    -1   -1   0   
$EndComp
$Comp
L Device:L_Small L501
U 1 1 594D218C
P 6150 900
F 0 "L501" H 6180 940 50  0000 L CNN
F 1 "22u" H 6180 860 50  0000 L CNN
F 2 "Inductor_SMD:L_1210_3225Metric" H 6150 900 50  0001 C CNN
F 3 "" H 6150 900 50  0000 C CNN
	1    6150 900 
	0    -1   -1   0   
$EndComp
$Comp
L Device:D_Schottky_Small D501
U 1 1 594D28B9
P 5700 1050
F 0 "D501" H 5650 1130 50  0000 L CNN
F 1 "B140" V 5800 1100 50  0000 L CNN
F 2 "Diode_SMD:D_SMB" V 5700 1050 50  0001 C CNN
F 3 "" V 5700 1050 50  0000 C CNN
	1    5700 1050
	0    1    1    0   
$EndComp
$Comp
L Connector:TestPoint W503
U 1 1 5954BEB2
P 9900 900
F 0 "W503" H 9900 1170 50  0000 C CNN
F 1 "3.3V" H 9900 1100 50  0000 C CNN
F 2 "TestPoint:TestPoint_Pad_D2.0mm" H 10100 900 50  0001 C CNN
F 3 "" H 10100 900 50  0000 C CNN
	1    9900 900 
	1    0    0    -1  
$EndComp
$Comp
L Connector:TestPoint W504
U 1 1 5954C679
P 10100 900
F 0 "W504" H 10100 1170 50  0000 C CNN
F 1 "3.3V" H 10100 1100 50  0000 C CNN
F 2 "TestPoint:TestPoint_Pad_D2.0mm" H 10300 900 50  0001 C CNN
F 3 "" H 10300 900 50  0000 C CNN
	1    10100 900 
	1    0    0    -1  
$EndComp
$Comp
L Connector:TestPoint W505
U 1 1 5954C721
P 10450 900
F 0 "W505" H 10450 1170 50  0000 C CNN
F 1 "GNDD" H 10450 1100 50  0000 C CNN
F 2 "TestPoint:TestPoint_Pad_D2.0mm" H 10650 900 50  0001 C CNN
F 3 "" H 10650 900 50  0000 C CNN
	1    10450 900 
	1    0    0    -1  
$EndComp
$Comp
L Connector:TestPoint W506
U 1 1 5954C7DE
P 10650 900
F 0 "W506" H 10650 1170 50  0000 C CNN
F 1 "GNDD" H 10650 1100 50  0000 C CNN
F 2 "TestPoint:TestPoint_Pad_D2.0mm" H 10850 900 50  0001 C CNN
F 3 "" H 10850 900 50  0000 C CNN
	1    10650 900 
	1    0    0    -1  
$EndComp
$Comp
L Connector:TestPoint W502
U 1 1 5954CA00
P 9700 900
F 0 "W502" H 9700 1170 50  0000 C CNN
F 1 "3.3V" H 9700 1100 50  0000 C CNN
F 2 "TestPoint:TestPoint_Pad_D2.0mm" H 9900 900 50  0001 C CNN
F 3 "" H 9900 900 50  0000 C CNN
	1    9700 900 
	1    0    0    -1  
$EndComp
$Comp
L Connector:TestPoint W501
U 1 1 5954CAC9
P 9500 900
F 0 "W501" H 9500 1170 50  0000 C CNN
F 1 "3.3V" H 9500 1100 50  0000 C CNN
F 2 "TestPoint:TestPoint_Pad_D2.0mm" H 9700 900 50  0001 C CNN
F 3 "" H 9700 900 50  0000 C CNN
	1    9500 900 
	1    0    0    -1  
$EndComp
$Comp
L power:GNDD #PWR0515
U 1 1 5954D153
P 10450 1000
F 0 "#PWR0515" H 10450 750 50  0001 C CNN
F 1 "GNDD" H 10450 850 50  0000 C CNN
F 2 "" H 10450 1000 50  0000 C CNN
F 3 "" H 10450 1000 50  0000 C CNN
	1    10450 1000
	1    0    0    -1  
$EndComp
Text Notes 8000 1400 2    28   ~ 0
RED
$Comp
L power:+3.3V #PWR0502
U 1 1 595939BB
P 2950 1150
F 0 "#PWR0502" H 2950 1000 50  0001 C CNN
F 1 "+3.3V" H 2950 1290 50  0000 C CNN
F 2 "" H 2950 1150 50  0000 C CNN
F 3 "" H 2950 1150 50  0000 C CNN
	1    2950 1150
	1    0    0    -1  
$EndComp
Wire Wire Line
	4350 4650 4350 4850
Wire Wire Line
	4450 4300 4450 4850
Connection ~ 4350 4850
Wire Wire Line
	4550 4850 4550 4300
Connection ~ 4450 4850
Wire Wire Line
	4650 4850 4650 4300
Connection ~ 4550 4850
Wire Wire Line
	4750 4850 4750 4300
Wire Wire Line
	4350 4300 4350 4450
Wire Wire Line
	3800 1200 3800 1300
Wire Wire Line
	3600 900  3800 900 
Wire Wire Line
	4300 900  4300 1250
Connection ~ 3800 900 
Wire Wire Line
	4400 800  4400 1250
Wire Wire Line
	4500 900  4500 1250
Connection ~ 4300 900 
Wire Wire Line
	4600 900  4600 1000
Connection ~ 4500 900 
Wire Wire Line
	4600 1250 4600 1200
Wire Wire Line
	3600 750  3600 900 
Wire Wire Line
	3600 1200 3600 1300
Wire Wire Line
	3600 1300 3800 1300
Connection ~ 3800 1300
Wire Wire Line
	4800 1250 4800 1200
Wire Wire Line
	4700 1250 4700 900 
Wire Wire Line
	4700 900  4800 900 
Wire Wire Line
	4800 900  4800 1000
Wire Wire Line
	5050 1250 5050 1200
Wire Wire Line
	5050 1000 5050 900 
Wire Wire Line
	5050 900  5150 900 
Wire Wire Line
	5150 900  5150 1250
Wire Wire Line
	6250 900  6550 900 
Wire Wire Line
	6550 900  6550 1000
Wire Wire Line
	7000 900  7000 1150
Connection ~ 6550 900 
Wire Wire Line
	7350 900  7350 1150
Connection ~ 7000 900 
Wire Wire Line
	6550 1200 6550 1250
Wire Wire Line
	6550 1250 5250 1250
Connection ~ 6550 1250
Wire Wire Line
	6550 1550 6550 1600
Wire Wire Line
	7000 1600 7000 1350
Wire Wire Line
	7350 1600 7350 1350
Connection ~ 7000 1600
Wire Wire Line
	5700 1150 5700 1500
Wire Wire Line
	5700 900  5700 950 
Connection ~ 5150 900 
Connection ~ 5700 900 
Connection ~ 7350 900 
Wire Wire Line
	8200 900  8200 850 
Wire Wire Line
	4100 1000 4100 900 
Connection ~ 4100 900 
Wire Wire Line
	4100 1300 4100 1200
Wire Wire Line
	2650 1850 3900 1850
Wire Wire Line
	2650 1950 3350 1950
Wire Wire Line
	2650 2150 3150 2150
Wire Wire Line
	2650 2350 2950 2350
Wire Wire Line
	3900 2450 2650 2450
Wire Wire Line
	3900 2550 2650 2550
Wire Wire Line
	3900 2650 2650 2650
Wire Wire Line
	3900 2850 2650 2850
Wire Wire Line
	3900 2950 2650 2950
Wire Wire Line
	3900 3050 2650 3050
Wire Wire Line
	3900 3150 2650 3150
Wire Wire Line
	3900 3250 2650 3250
Wire Wire Line
	3900 3350 2650 3350
Wire Wire Line
	2650 3550 3900 3550
Wire Wire Line
	2650 3650 3900 3650
Wire Wire Line
	2650 3750 3900 3750
Connection ~ 2950 2350
Connection ~ 3600 900 
Wire Wire Line
	3800 900  3800 1000
Connection ~ 3150 2150
Wire Wire Line
	6400 3350 6400 4650
Wire Wire Line
	6400 4650 7200 4650
Wire Wire Line
	5600 3250 6500 3250
Wire Wire Line
	6500 3250 6500 4450
Wire Wire Line
	6500 4450 7200 4450
Wire Wire Line
	5600 3550 6300 3550
Wire Wire Line
	6300 3550 6300 4750
Wire Wire Line
	6300 4750 8150 4750
Wire Wire Line
	5600 3650 6200 3650
Wire Wire Line
	6200 3650 6200 4950
Wire Wire Line
	6200 4950 8150 4950
Wire Wire Line
	5600 3850 6100 3850
Wire Wire Line
	6100 3850 6100 5050
Wire Wire Line
	6100 5050 9150 5050
Wire Wire Line
	9150 5250 6000 5250
Wire Wire Line
	6000 5250 6000 3950
Wire Wire Line
	6000 3950 5600 3950
Wire Wire Line
	9150 5350 9150 5250
Wire Wire Line
	7200 5350 8150 5350
Wire Wire Line
	8150 4950 8150 5350
Connection ~ 9150 5350
Wire Wire Line
	7200 4650 7200 5350
Connection ~ 8150 5350
Wire Wire Line
	9150 1400 9150 1550
Wire Wire Line
	5600 1850 7200 1850
Wire Wire Line
	8600 1850 8600 1550
Wire Wire Line
	8600 1550 9150 1550
Connection ~ 9150 1550
Wire Wire Line
	5600 2050 8750 2050
Wire Wire Line
	8750 2050 8750 1850
Wire Wire Line
	8750 1850 8850 1850
Wire Wire Line
	9150 2050 9150 2150
Wire Wire Line
	8150 2900 8150 3000
Wire Wire Line
	7200 3750 7200 3850
Wire Wire Line
	7200 4450 7200 4350
Wire Wire Line
	5600 3050 6600 3050
Wire Wire Line
	6600 3050 6600 4150
Wire Wire Line
	6600 4150 6900 4150
Wire Wire Line
	6700 3850 7200 3850
Wire Wire Line
	6700 3850 6700 2950
Wire Wire Line
	6700 2950 5600 2950
Connection ~ 7200 3850
Wire Wire Line
	5600 2850 6800 2850
Wire Wire Line
	6800 2850 6800 3550
Wire Wire Line
	6800 3550 6900 3550
Wire Wire Line
	5600 2450 7750 2450
Wire Wire Line
	7750 2450 7750 2700
Wire Wire Line
	7750 2700 7850 2700
Wire Wire Line
	5600 2550 7650 2550
Wire Wire Line
	7650 2550 7650 3000
Wire Wire Line
	7650 3000 8150 3000
Connection ~ 8150 3000
Wire Wire Line
	5600 2650 7550 2650
Wire Wire Line
	7550 2650 7550 3300
Wire Wire Line
	7550 3300 7850 3300
Wire Wire Line
	7200 3350 7200 1850
Connection ~ 7200 1850
Wire Wire Line
	8150 2500 8150 1850
Connection ~ 8150 1850
Wire Wire Line
	8150 4750 8150 3500
Wire Wire Line
	5600 2150 9150 2150
Connection ~ 9150 2150
Wire Wire Line
	5600 2250 8750 2250
Wire Wire Line
	8750 2250 8750 2450
Wire Wire Line
	8750 2450 8850 2450
Wire Wire Line
	9150 5050 9150 2650
Wire Wire Line
	2950 1150 2950 1250
Connection ~ 2950 1250
Wire Wire Line
	10100 4350 10100 4450
Wire Wire Line
	10100 4450 10400 4450
Wire Wire Line
	10400 4450 10400 4550
Connection ~ 10100 4450
Wire Wire Line
	10700 4450 10700 4550
Connection ~ 10400 4450
Wire Wire Line
	10700 4750 10700 4850
Wire Wire Line
	10100 4750 10100 4850
Wire Wire Line
	10100 4850 10400 4850
Connection ~ 10700 4850
Wire Wire Line
	10400 4750 10400 4850
Connection ~ 10400 4850
Wire Wire Line
	10000 2900 10650 2900
Wire Wire Line
	10000 3850 10000 3100
Wire Wire Line
	10000 3100 10650 3100
Wire Wire Line
	9600 5350 9600 5500
Wire Wire Line
	4350 4850 4450 4850
Connection ~ 4650 4850
Wire Wire Line
	10200 6050 10200 5850
Wire Wire Line
	10200 5850 10550 5850
Wire Wire Line
	10550 5850 10550 6050
Wire Wire Line
	6550 1600 7000 1600
Connection ~ 6550 1600
Wire Wire Line
	3900 6200 3900 6450
Wire Wire Line
	3050 6450 3900 6450
Wire Wire Line
	3650 6200 3650 6550
Wire Wire Line
	3400 6200 3400 6650
Wire Wire Line
	2300 6600 2300 6800
Wire Wire Line
	2900 6750 3150 6750
Wire Wire Line
	3150 6750 3150 6200
Wire Wire Line
	3900 5850 3900 5950
Wire Wire Line
	3150 5950 3400 5950
Wire Wire Line
	3150 5950 3150 6000
Wire Wire Line
	3400 6000 3400 5950
Connection ~ 3400 5950
Wire Wire Line
	3650 6000 3650 5950
Connection ~ 3650 5950
Connection ~ 3900 5950
Wire Wire Line
	3050 6100 3050 6450
Wire Wire Line
	1400 6100 1700 6100
Connection ~ 3900 6450
Connection ~ 3650 6550
Connection ~ 3400 6650
Connection ~ 3150 6750
Wire Wire Line
	1400 6500 2100 6500
Wire Wire Line
	2100 6500 2100 6800
Wire Wire Line
	1400 6400 1900 6400
Wire Wire Line
	1900 6400 1900 6800
Wire Wire Line
	1700 6100 1700 6800
Wire Wire Line
	1700 7000 1700 7100
Wire Wire Line
	1700 7100 1900 7100
Wire Wire Line
	2300 7000 2300 7100
Wire Wire Line
	2100 7100 2100 7000
Connection ~ 2100 7100
Wire Wire Line
	1900 7100 1900 7000
Connection ~ 1900 7100
Connection ~ 2300 7100
Connection ~ 1700 6100
Connection ~ 1900 6400
Connection ~ 2100 6500
Connection ~ 2300 6600
Wire Wire Line
	4050 5950 4050 6350
Wire Wire Line
	4050 7100 4050 6850
Wire Wire Line
	7850 1600 7850 1500
Connection ~ 7350 1600
Wire Wire Line
	7850 1300 7850 1200
Wire Wire Line
	7850 1000 7850 900 
Connection ~ 7850 900 
Connection ~ 8200 900 
Connection ~ 9500 900 
Connection ~ 9700 900 
Connection ~ 9900 900 
Wire Wire Line
	10450 900  10450 950 
Wire Wire Line
	10450 950  10650 950 
Wire Wire Line
	10650 950  10650 900 
Connection ~ 10450 950 
Wire Wire Line
	3000 6550 3650 6550
Wire Wire Line
	3000 6550 3000 6400
Wire Wire Line
	3000 6400 2900 6400
Wire Wire Line
	2950 6650 3400 6650
Wire Wire Line
	2950 6650 2950 6500
Wire Wire Line
	2950 6500 2900 6500
Wire Wire Line
	1400 6600 2300 6600
Wire Wire Line
	10000 2150 10000 2900
Connection ~ 3350 1950
Wire Wire Line
	2950 1250 3150 1250
Connection ~ 3150 1250
Wire Wire Line
	3350 1650 3350 1950
Wire Wire Line
	3150 1650 3150 2150
Wire Wire Line
	2950 1650 2950 2350
Wire Wire Line
	3150 1250 3150 1450
Wire Wire Line
	3350 1250 3350 1450
$Comp
L Device:R_Small R504
U 1 1 595908D3
P 3350 1550
F 0 "R504" H 3380 1570 50  0000 L CNN
F 1 "10k" H 3380 1510 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 3350 1550 50  0001 C CNN
F 3 "" H 3350 1550 50  0000 C CNN
	1    3350 1550
	-1   0    0    -1  
$EndComp
$Comp
L Device:R_Small R502
U 1 1 593CD212
P 3150 1550
F 0 "R502" H 3180 1570 50  0000 L CNN
F 1 "10k" H 3180 1510 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 3150 1550 50  0001 C CNN
F 3 "" H 3150 1550 50  0000 C CNN
	1    3150 1550
	-1   0    0    -1  
$EndComp
$Comp
L Device:R_Small R501
U 1 1 593CC07F
P 2950 1550
F 0 "R501" H 2980 1570 50  0000 L CNN
F 1 "10k" H 2980 1510 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 2950 1550 50  0001 C CNN
F 3 "" H 2950 1550 50  0000 C CNN
	1    2950 1550
	-1   0    0    -1  
$EndComp
Text Notes 4050 650  3    28   ~ 0
Place next to VIN
NoConn ~ 2500 6300
NoConn ~ 2900 6300
$Comp
L Device:R_Pack04 RP501
U 1 1 59545D47
P 2700 6500
F 0 "RP501" H 2950 6450 50  0000 C CNN
F 1 "10k" H 2950 6550 50  0000 C CNN
F 2 "Resistor_SMD:R_Array_Concave_4x0603" H 2700 6500 50  0001 C CNN
F 3 "" H 2700 6500 50  0000 C CNN
	1    2700 6500
	0    1    1    0   
$EndComp
Wire Wire Line
	5600 3350 6400 3350
$Comp
L power:PWR_FLAG #FLG0501
U 1 1 59672D42
P 8650 850
F 0 "#FLG0501" H 8650 945 50  0001 C CNN
F 1 "PWR_FLAG" H 8650 1030 50  0000 C CNN
F 2 "" H 8650 850 50  0000 C CNN
F 3 "" H 8650 850 50  0000 C CNN
	1    8650 850 
	1    0    0    -1  
$EndComp
Wire Wire Line
	8650 850  8650 900 
Connection ~ 8650 900 
$Comp
L Device:Q_NMOS_GDS Q501
U 1 1 596856EF
P 7100 3550
F 0 "Q501" H 7400 3600 50  0000 R CNN
F 1 "IRFS7530" H 7650 3500 50  0000 R CNN
F 2 "Package_TO_SOT_SMD:TO-263-2" H 7300 3650 50  0001 C CNN
F 3 "" H 7100 3550 50  0000 C CNN
	1    7100 3550
	1    0    0    -1  
$EndComp
$Comp
L Device:Q_NMOS_GDS Q502
U 1 1 59685C29
P 7100 4150
F 0 "Q502" H 7400 4200 50  0000 R CNN
F 1 "IRFS7530" H 7650 4100 50  0000 R CNN
F 2 "Package_TO_SOT_SMD:TO-263-2" H 7300 4250 50  0001 C CNN
F 3 "" H 7100 4150 50  0000 C CNN
	1    7100 4150
	1    0    0    -1  
$EndComp
$Comp
L Device:Q_NMOS_GDS Q504
U 1 1 59685CEB
P 8050 3300
F 0 "Q504" H 8350 3350 50  0000 R CNN
F 1 "IRFS7530" H 8600 3250 50  0000 R CNN
F 2 "Package_TO_SOT_SMD:TO-263-2" H 8250 3400 50  0001 C CNN
F 3 "" H 8050 3300 50  0000 C CNN
	1    8050 3300
	1    0    0    -1  
$EndComp
$Comp
L Device:Q_NMOS_GDS Q503
U 1 1 59685DC8
P 8050 2700
F 0 "Q503" H 8350 2750 50  0000 R CNN
F 1 "IRFS7530" H 8600 2650 50  0000 R CNN
F 2 "Package_TO_SOT_SMD:TO-263-2" H 8250 2800 50  0001 C CNN
F 3 "" H 8050 2700 50  0000 C CNN
	1    8050 2700
	1    0    0    -1  
$EndComp
$Comp
L Device:Q_NMOS_GDS Q506
U 1 1 59685E8A
P 9050 2450
F 0 "Q506" H 9350 2500 50  0000 R CNN
F 1 "IRFS7530" H 9600 2400 50  0000 R CNN
F 2 "Package_TO_SOT_SMD:TO-263-2" H 9250 2550 50  0001 C CNN
F 3 "" H 9050 2450 50  0000 C CNN
	1    9050 2450
	1    0    0    -1  
$EndComp
$Comp
L Device:Q_NMOS_GDS Q505
U 1 1 59685F59
P 9050 1850
F 0 "Q505" H 9350 1900 50  0000 R CNN
F 1 "IRFS7530" H 9600 1800 50  0000 R CNN
F 2 "Package_TO_SOT_SMD:TO-263-2" H 9250 1950 50  0001 C CNN
F 3 "" H 9050 1850 50  0000 C CNN
	1    9050 1850
	1    0    0    -1  
$EndComp
Text Notes 7400 4600 0    28   ~ 0
Connect sense traces\nwith Kelvin termination
Text Notes 8350 4900 0    28   ~ 0
Connect sense traces\nwith Kelvin termination
Text Notes 9350 5200 0    28   ~ 0
Connect sense traces\nwith Kelvin termination
Wire Notes Line
	500  5350 5350 5350
Wire Notes Line
	5350 5350 5350 7800
Text Notes 600  5550 0    55   ~ 0
Hall sensor input
Wire Wire Line
	4350 4850 4350 4950
Wire Wire Line
	4450 4850 4550 4850
Wire Wire Line
	4550 4850 4650 4850
Wire Wire Line
	3800 900  4100 900 
Wire Wire Line
	4300 900  4500 900 
Wire Wire Line
	4500 900  4600 900 
Wire Wire Line
	3800 1300 3800 1400
Wire Wire Line
	3800 1300 4100 1300
Wire Wire Line
	6550 900  7000 900 
Wire Wire Line
	7000 900  7350 900 
Wire Wire Line
	6550 1250 6550 1350
Wire Wire Line
	7000 1600 7350 1600
Wire Wire Line
	5150 900  5700 900 
Wire Wire Line
	5700 900  6050 900 
Wire Wire Line
	7350 900  7850 900 
Wire Wire Line
	4100 900  4300 900 
Wire Wire Line
	2950 2350 3900 2350
Wire Wire Line
	3600 900  3600 1000
Wire Wire Line
	3150 2150 3900 2150
Wire Wire Line
	9150 5350 9600 5350
Wire Wire Line
	8150 5350 9150 5350
Wire Wire Line
	9150 1550 9150 1650
Wire Wire Line
	7200 3850 7200 3950
Wire Wire Line
	7200 3850 10000 3850
Wire Wire Line
	8150 3000 8150 3100
Wire Wire Line
	8150 3000 10650 3000
Wire Wire Line
	7200 1850 8150 1850
Wire Wire Line
	8150 1850 8600 1850
Wire Wire Line
	9150 2150 9150 2250
Wire Wire Line
	9150 2150 10000 2150
Wire Wire Line
	2950 1250 2950 1450
Wire Wire Line
	10100 4450 10100 4550
Wire Wire Line
	10400 4450 10700 4450
Wire Wire Line
	10700 4850 10700 4950
Wire Wire Line
	10400 4850 10700 4850
Wire Wire Line
	4650 4850 4750 4850
Wire Wire Line
	6550 1600 6550 1650
Wire Wire Line
	3400 5950 3650 5950
Wire Wire Line
	3650 5950 3900 5950
Wire Wire Line
	3900 5950 3900 6000
Wire Wire Line
	3900 5950 4050 5950
Wire Wire Line
	3900 6450 4050 6450
Wire Wire Line
	3650 6550 4050 6550
Wire Wire Line
	3400 6650 4050 6650
Wire Wire Line
	3150 6750 4050 6750
Wire Wire Line
	2100 7100 2300 7100
Wire Wire Line
	1900 7100 2100 7100
Wire Wire Line
	2300 7100 2300 7200
Wire Wire Line
	2300 7100 4050 7100
Wire Wire Line
	1700 6100 3050 6100
Wire Wire Line
	1900 6400 2500 6400
Wire Wire Line
	2100 6500 2500 6500
Wire Wire Line
	2300 6600 2500 6600
Wire Wire Line
	7350 1600 7850 1600
Wire Wire Line
	7850 900  8200 900 
Wire Wire Line
	8200 900  8650 900 
Wire Wire Line
	9500 900  9700 900 
Wire Wire Line
	9700 900  9900 900 
Wire Wire Line
	9900 900  10100 900 
Wire Wire Line
	10450 950  10450 1000
Wire Wire Line
	3350 1950 3900 1950
Wire Wire Line
	3150 1250 3350 1250
Wire Wire Line
	8650 900  9500 900 
Wire Wire Line
	2900 6600 2900 6750
$EndSCHEMATC
